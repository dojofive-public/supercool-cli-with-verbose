#mysupercoolscript.py
    
import os
import click
import sys

import logging

cool_logger = logging.getLogger('cool_logger')
cl = logging.StreamHandler()
cl.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(message)s')
cl.setFormatter(formatter)
cool_logger.addHandler(cl)
    
@click.group()
@click.option('--verbose', '-v', is_flag=True, help="Will print more logging messages.")
@click.pass_context
def mysupercoolscript(self, verbose):
  if verbose:
    cool_logger.setLevel(logging.DEBUG)
  else:
    cool_logger.setLevel(logging.INFO)
  pass
  
@mysupercoolscript.command()
def useful_command():
    """Does something useful"""
    cool_logger.debug("Cool. You're debugging so you see me.")
    cool_logger.info("You always get to see me.")
 
cli = click.CommandCollection(sources=[mysupercoolscript])
 
if __name__ == '__main__':
    mysupercoolscript()