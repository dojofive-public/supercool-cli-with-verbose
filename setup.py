#setup.py

from setuptools import setup
setup(
    name='mysupercoolscript',
    version='0.0.1',
    entry_points={
        'console_scripts': [
            'mysupercoolscript=mysupercoolscript:mysupercoolscript'
        ]
    }
)